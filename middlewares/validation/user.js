const validator = require('validator');
const { User } = require('../../models/User');
const _ = require('lodash');

module.exports.validateCreateUser = async (req, res, next) => {
    let errors = {};

    //Validate
    const email = _.get(req, "body.email", "");
    const name = _.get(req, "body.name", "");
    const password = _.get(req, "body.password", "");
    const password2 = _.get(req, "body.password2", "");

    if (validator.isEmpty(email)) {
        errors.email = "Email is required";
    } else {
        const user = await User.findOne({ email })
        if (user) {
            errors.email = "Email exists"
        } else if (!validator.isEmail(email)) {
            errors.email = "Email is invalid"
        }
    }

    //Name
    if (validator.isEmpty(name)) {
        errors.name = "Name is required";
    }

    //Password
    if (validator.isEmpty(password)) {
        errors.password = "Password is required";
    } else if (!validator.isLength(password, { min: 8 })) {
        errors.password = "Password must have at least 8 characters"
    }

    //Password2
    if (validator.isEmpty(password2)) {
        errors.password2 = "Confirmed password is required"
    } else if (!validator.equals(password, password2)) {
        errors.password2 = "Password must match"
    }

    const isValid = _.isEmpty(errors)
    if (isValid) return next();
    res.status(400).json(errors)
}