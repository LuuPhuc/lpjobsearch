// const LocalStrategy = require('passport-local').Strategy;
// const bcrypt = require('bcryptjs');

// //User
// const { User } = require('../../models/User');

// module.exports.passportConfig = async (passport) => {
//     try {
//         passport.use(
//             new LocalStrategy({ usernameField: 'email' }, (email, password, done) => {
//                 const user = await User.findOne({ email: email })
//                 if (!user) return done(null, false, { message: "Email already exists" });

//                 //Match password
//                 bcrypt.compare(password, user.password, (err, isMatch) => {
//                     if (err) return res.status(500).json(err);
//                     if (isMatch) {
//                         return done(null, user);
//                     } else {
//                         return done(null, false, { message: "Password incorrect" });
//                     }
//                 })
//             })
//         )
//         passport.serializeUser(function (user, done) {
//             done(null, user.id);
//         });

//         passport.deserializeUser(function (id, done) {
//             User.findById(id, function (err, user) {
//                 done(err, user)
//             });
//         });
//     } catch (e) {
//         console.log(e)
//     }
// };
