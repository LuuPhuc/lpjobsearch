const jwt = require('jsonwebtoken');
const config = require('../../config/');

// module.exports.ensureAuthenticated = (req, res, next) => {
//     if (req.isAuthenticated()) return next();
//     res.redirect('/user/login')
// }

// module.exports.forwardAuthenticated = (req, res, next) => {
//     if (!req.isAuthenticated()) return next();
//     res.redirect('/homepage')
// }

module.exports.createToken = (user, expire) => {
    return jwt.sign(user, config.SecretKey, {
        expiresIn: expire
    })
}
module.exports.authorize = (userTypeArray) => (req, res, next) => {
    const { userType } = req.user;
    const index = userTypeArray.findIndex(e => e === userType)
    console.log(index);
    if (index > -1) return next();
    res.status(403).json({ message: "You are not allowed to access" })
}