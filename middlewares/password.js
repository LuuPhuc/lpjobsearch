const bcrypt = require('bcryptjs');

// exports.cryptPassword = async (plaintextPassword, res) => {
//     try {
//         await bcrypt.genSalt(10, (err, salt) => {
//             if (err) return res.status(401).json(err);
//             bcrypt.hash(plaintextPassword, salt, (err, hash) => {
//                 if (err) return res.status(401).json(err);
//                 return hash;
//             })
//         })
//     } catch (e) {
//         console.log(e)
//     }
// }

exports.cryptPassword = (plaintextPassword, res) => {
    return new Promise((resolve, reject) => {
        bcrypt.genSalt(10, (err, salt) => {
            if (err) return reject(err);
            bcrypt.hash(plaintextPassword, salt, (err, hash) => {
                if (err) return reject(err);
                return resolve(hash);
            });
        });
    })
}

exports.comparePassword = (plaintextPassword, hash) => {
    return new Promise((resolve, reject) => {
        bcrypt.compare(plaintextPassword, hash, (err, reusult) => {
           if(err) return reject(err)
           return resolve(reusult)
        })
    })
}
