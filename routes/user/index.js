const express = require('express');
const userController = require('./user');

const router = express.Router();

const { validateCreateUser } = require("../../middlewares/validation/user");
const { authorize } = require("../../middlewares/passport/auth")

router.post('/register', validateCreateUser, userController.userRegister)
router.post('/login', authorize([0]),userController.userLogin)

module.exports = router;