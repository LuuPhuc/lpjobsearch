const { User } = require('../../models/User');
const passwordConfig = require('../../middlewares/password')
// const { passportConfig } = require('../../middlewares/passport/passport')
const { createToken } = require('../../middlewares/passport/auth')

module.exports.userRegister = async (req, res, next) => {
    const { name, email, password } = req.body;
    try {
        const passwordHash = await passwordConfig.cryptPassword(password);
        const newUser = new User({
            name, email, password: passwordHash
        })
        newUser.save();
        return res.status(200).json({ message: "Register successful" })
    } catch (e) {
        console.log(e);
    }
}
module.exports.userLogin = (req, res, next) => {
    const { email, password } = req.body;
    User.findOne({ email })
        .then(user => {
            if (!user) return Promise.reject({ message: "Email do not exists" })
            let tokenPayLoad = { id: user._id, email: user.email }
            return Promise.all([
                passwordConfig.comparePassword(password, user.password),
                createToken(tokenPayLoad, "3 day")
            ])
        })
        .then(result => {
            let accessToken = result[1];
            if (!result[0]) return res.status(401).json({ message: "Password is not match" })
            return res.status(200).json({ accessToken, email })
        })
        .catch(err => {
            return res.status(401).json(err)
        })
}