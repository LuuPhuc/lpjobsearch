const mongoose = require('mongoose');
const UserTypeEnum = require('../enum/UserTypeEnum')

const UserSchema = new mongoose.Schema({
    userType: {
        type: String,
        require: true,
        default: 0
    },
    name: {
        type: String,
        require: String
    },
    email: {
        type: String,
        unique: true,
        require: true
    },
    password: {
        type: String,
        require: true
    },
    avatar: {
        type: String,
        require: false,
        default: null
    }
})

const User = mongoose.model("User", UserSchema, "User")

module.exports = {
    UserSchema, User
}