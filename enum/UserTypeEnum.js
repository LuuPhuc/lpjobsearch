module.exports = Object.freeze({
    TYPE: {
        ADMIN: 0,
        JOBSEEKER: 1,
        ANONYMOUS: 2
    }
})