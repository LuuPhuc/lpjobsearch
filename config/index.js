const dotenv = require('dotenv');
dotenv.config();

let MongoURI;
let PORT;
let SecretKey;
let Email;

switch (process.env.NODE_ENV) {
    case "local":
        MongoURI = process.env.LOCAL_MONGODB_URI
        PORT = process.env.LOCAL_PORT
        SecretKey = process.env.LOCAL_SECRET_KEY
        Email = process.env.LOCAL_EMAIL
        break;
}

module.exports = {
    MongoURI, PORT, SecretKey, Email
}