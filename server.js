const express = require('express');
const mongoose = require('mongoose');
const srvConfig = require('./config');
const app = express();

app.use(express.json())

// connect to mongoDB
mongoose.connect(srvConfig.MongoURI, { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true })
    .then(() => console.log("Connect Database successful"))
    .catch(err => console.log(err))

app.use("/api", require("./routes"));

app.listen(srvConfig.PORT, () => {
    console.log(`Server start successful on ${srvConfig.PORT}`)
})
